# Development


## Versioning

There is no versioning in this project right now.

Only take images from :master unless you really want a dev version.
Then consider taking a specific hash unless you're tracking a
development branch.


## Jenkins

*   TODO: Get it working with 'agent && docker' node label.
    Right now that current label, the docker daemon cannot be reached.
    Need to look at Docker image I'm using for that label combination.



## jenkins.agent image

The jenkins.agent image is the cleanest from a dev standpoint.

Trying to use that as a template for 'the smart thing'.


### --cache-from

Maybe rip out the --cache-from options in the build script?

Or take the cache from $(image):latest ?


