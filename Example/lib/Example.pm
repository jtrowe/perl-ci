package Example;

# ABSTRACT: An example package

use Moo;


has 'user' => (
    is      => 'ro',
    default => sub { return 'username'; },
);

has 'pass' => (
    is      => 'ro',
    default => sub { return 'password'; },
);


sub auth {
    my $self  = shift;
    my $user  = shift;
    my $pass  = shift;

    if ( $self->pass eq $pass ) {
        return 1;
    }

    return 0;
}


1;
