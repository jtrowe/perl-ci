use strict;
use warnings;

use Test::More;

use Example;

plan(tests => 4);

my $e = new_ok('Example');


ok($e->auth('username', 'password'), 'auth works');

ok(! $e->auth('username', 'foo'), 'auth fails as expected');

my $maybe_fail = $ENV{EXAMPLE_FAIL} ? $ENV{EXAMPLE_FAIL} : 0;
if ( $maybe_fail =~ m/false/i ) {
    $maybe_fail = 0;
}
ok(! $maybe_fail, 'Example Test Failure');


