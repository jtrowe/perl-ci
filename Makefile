
MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --warn-undefined-variables

.DELETE_ON_ERROR:


owner             := jtrowe
project           := perl-ci


.PHONEY : build
build : build.dev.debian build.agent build.base build.dzil


.PHONEY : build.agent
build.agent :
	cd jenkins.agent && PATH=$$PWD/ci/bin:$$PWD/../ci/bin:$$PATH $(MAKE) build


.PHONEY : build.base
build.base :
	- cp --archive jenkins.agent/.image.push jenkins.base/.base.image
	cd jenkins.base && PATH=$$PWD/ci/bin:$$PWD/../ci/bin:$$PATH $(MAKE) build


.PHONEY : build.dev.debian
build.dev.debian :
	cd dev.debian && PATH=$$PWD/ci/bin:$$PWD/../ci/bin:$$PATH $(MAKE) build


build.dzil :
	- cp --archive jenkins.base/.image.push jenkins.dzil/.base.image
	cd jenkins.dzil && PATH=$$PWD/ci/bin:$$PWD/../ci/bin:$$PATH $(MAKE) build


.PHONEY: deploy
deploy : deploy.base deploy.dzil


.PHONEY: deploy.base
deploy.base :
	cd jenkins.base && PATH=$$PWD/ci/bin:$$PATH $(MAKE) deploy


.PHONEY: deploy.dzil
deploy.dzil :
	cd jenkins.dzil && PATH=$$PWD/ci/bin:$$PATH $(MAKE) deploy


.PHONEY: deploy.latest
deploy.latest :
	cd jenkins.base && PATH=$$PWD/ci/bin:$$PATH $(MAKE) deploy.latest
	cd jenkins.dzil && PATH=$$PWD/ci/bin:$$PATH $(MAKE) deploy.latest


