# Perl-CI

Provides a Docker image for CI testing of Perl code.

Note: This is mostly for my own use.  User beware.


## Images

### gitlab/base

A Perl image that can be used to test Perl packages with GitLab CI.


### gitlab/dzil

A Perl image that can be uses Dist-Zilla to build Perl packages
with GitLab CI.
Extends the gitlab/base image; since that one has Perl installed
already.


### jenkins/base

A Perl image that can be used to test Perl packages with Jenkins.
This is tuned for my home lab.


### jenkins/dzil

A Perl image that can be uses Dist-Zilla to build Perl packages
with Jenkins.
Extends the Jenkins/base image; since that one has Perl installed
already.
This is tuned for my home lab.


## GitLab

Probably eventually want to actually have a runner that has most of the tooling
that I expect.


## TODO

### jenkins

*   Figure out how to let docker decide to rebuild the image or not,
    then perhaps perform other tasks (like include build.yml).


## Thanks To

*   https://gitlab.com/rsrchboy/perl-ci
    This borrowed very heavily from this work for the GitLab CI stuff.

