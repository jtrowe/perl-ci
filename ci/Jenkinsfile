
def base_image_name
def dzil_image_name

pipeline {

    agent {
        label 'agent && docker'
    }

    environment {
        PATH="${env.WORKSPACE}/ci/bin:${env.PATH}"
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '5'))
    }

    parameters {

        booleanParam(
            defaultValue: false,
            description: 'Deploy the Image',
            name: 'DEPLOY'
        )

        booleanParam(
            defaultValue: false,
            description: 'Triggers an failure in the example test',
            name: 'EXAMPLE_FAIL'
        )

        booleanParam(
            defaultValue: false,
            description: 'Ignore Docker Cache',
            name: 'IGNORE_CACHE'
        )

    }

    stages {

        stage('build.dev.debian') {

            steps {
                sh '''
                    make build.dev.debian registry="$DOCKER_REGISTRY"
                '''
            }

        }

        stage('build.agent') {

            steps {
                sh '''
                    set +e
                    rm -f jenkins.agent/image.push
                    rm -f jenkins.base/image.push
                    rm -f jenkins.base/.base.image
                    rm -f jenkins.dzil/.base.image
                    set -e
                    make build.agent registry="$DOCKER_REGISTRY"
                '''
            }

        }

        stage('build.base') {

            steps {
                sh '''
                    make build.base registry="$DOCKER_REGISTRY"
                '''

                script {
                    base_image_name = sh(
                        script: "cat jenkins.base/.image.tag",
                        returnStdout: true
                    )

                    print "base_image_name => $base_image_name"
                }
            }

        }

        stage('deploy.base') {

            when {

                anyOf {
                    branch 'FAKE'
                    expression {
                        return params.DEPLOY
                    }
                }

            }


            steps {
                sh '''
                    make deploy.base registry="$DOCKER_REGISTRY"
                '''

            }

        }

        stage('build.dzil') {

            steps {
                sh '''
                    make build.dzil registry="$DOCKER_REGISTRY"
                '''

                script {
                    dzil_image_name = sh(
                        script: "cat jenkins.dzil/.image.tag",
                        returnStdout: true
                    )

                    print "dzil_image_name => $dzil_image_name"
                }

            }

        }

        stage('test.dzil') {
            agent {
                docker {
                    image "${DOCKER_REGISTRY}/${dzil_image_name}"
                    registryUrl "http://${DOCKER_REGISTRY}"
                }
            }

            steps {
                sh '''
                    tar --extract --file /Example.tar
                    cd Example

                    #ls -al
                    #find build -type f -ls

                    dzil build
                    cd ..

                    #ls -al Example/
                    #find Example/ -type f -ls
                '''

                stash(
                    name: 'example-package',
                    includes: 'Example/Example-0.001.tar.gz'
                )

            }

        }

        stage('test.base') {
            agent {
                docker {
                    image "${DOCKER_REGISTRY}/${base_image_name}"
                    registryUrl "http://${DOCKER_REGISTRY}"
                }
            }

            environment {
                EXAMPLE_FAIL="${params.EXAMPLE_FAIL}"
            }

            steps {
                sh '''
                    ls -al
                    rm -rf Example build
                '''

                unstash 'example-package'

                script {
                    catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                        sh '''
                            ls -al
                            #ls -al Example/

                            echo "jenkinsfile: EXAMPLE_FAIL => $EXAMPLE_FAIL"

                            cd Example
                            test-perl-package Example-0.001.tar.gz
                            test_exit=$?

                            echo "test_exit => $test_exit"

                            ls -al
                            find . -type f -ls
                        '''
                    }

                    stash(
                        name: 'example-package-test-results',
                        includes: 'Example/build/cover_db/**, Example/build/tap/**/*.xml'
                    )

                    if (currentBuild.result == 'FAILURE') {
                        error('Stage Failed')
                    }

                }
            }

        }

        stage('deploy.dzil') {

            when {

                anyOf {
                    branch 'FAKE'
                    expression {
                        return params.DEPLOY
                    }
                }

            }

            steps {
                sh '''
                    make deploy.dzil registry="$DOCKER_REGISTRY"
                '''

            }

        }

        stage('deploy.latest') {

            when {

                anyOf {
                    branch 'FAKE'
                    expression {
                        return params.DEPLOY
                    }
                }

            }

            steps {
                sh '''
                    make deploy.latest registry="$DOCKER_REGISTRY"
                '''

            }

        }

    }

    post {
        always {
            unstash 'example-package-test-results'

            archiveArtifacts(
                artifacts: 'dev.debian/.image.json'
            )

            junit(
                allowEmptyResults: true,
                checksName: 'Example Package Test Results', 
                keepLongStdio: true,
                skipPublishingChecks: true,
                testResults: 'Example/build/tap/**/*.xml'
            )

            publishHTML(
                allowMissing: true,
                alwaysLinkToLastBuild: false,
                keepAll: true,
                reportDir: 'Example/build/cover_db',
                reportFiles: 'coverage.html',
                reportName: 'Example Package Test Coverage'
            )

        }
        success {
            cleanWs()
        }
    }

}

